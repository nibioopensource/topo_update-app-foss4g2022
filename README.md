# Workshop and talk at [FOSS4G 2023 Prizren](https://2023.foss4g.org/)

- [Use Postgis Topology simplify/secure work with spatial data.](https://talks.osgeo.org/foss4g-2023-workshop/talk/YCJ3YZ/) 
  - At "2023-06-27, 14:00–16:00 (Europe/Tirane), UBT D / N113 - Second Floor"

- [ More correct maps/data with Postgis Topology rather then Simple Feature ?](https://talks.osgeo.org/foss4g-2023-workshop/talk/YCJ3YZ/)
  - At "2023-06-29, 14:30–15:00 (Europe/Tirane), UBT C / N110 - Second Floor"



# Topology Update App

This repository contains the scripts to setup a `topo_update_rest`
based application for a foss4g demo.

It uses submodules so please remember to call:

    git submodule update --init

## Setting up the demo

This implies:

       1. Preparing a PostgreSQL Database
       2. Starting PostgREST
       3. Serving the client

### Preparing the database

A demo database can be created and prepared with:

```sh
./prepare_db.sh foss4g_dev
```

The script will:

  - Create a database with the given name
  - Load `topo_update_rest` support and all dependencies
  - Define a `topo_update` application named `foss4g`
  - Ensures the following PostgreSQL roles exist:
    - `topology_update_app-foss4g-editor`
      has privileges to fully use the `foss4g`
      application (read and write data)
    - `topology_update_app-foss4g-unauthenticated`
      has no specific privileges
    - `topology_update_app-foss4g-authenticator`
      has the privilege to switch either to the "editor"
      or "unauthenticated" roles above.

Standard libpq variables (except PGDATABASE) will be
honoured by this script and can be used to tweak connection
parameters., refer to:
https://www.postgresql.org/docs/current/libpq-envars.html

Valid state of a prepared database can be checked with:

```sh
./test_db.sh  foss4g_dev
```

Checking the database requires pgtap extension being available
on the target database system and the `pg_prove` command
(provided by `libtap-parser-sourcehandler-pgtap-perl` on
debian-derivate systems) available on the host making the call.

### Starting PostgREST

An instance of postgrest listening on a random available tcp port
of localhost can be started with:

    ./start_postgrest.sh foss4g_dev

The script will print information on standard in
shell variables assignment form:

    PID=44844
    LOGFILE=/tmp/postgrest-44841.log
    PORT=36771

Standard libpq variables (except PGDATABASE) will be
honoured by this script and can be used to tweak connection
parameters., refer to:
https://www.postgresql.org/docs/current/libpq-envars.html

You must make sure that the user you connect to the database as is
granted the `foss4g-authenticator-user` role.

### Serving the client

An webserver instance serving the client can be started with:

    ./start_webgui.sh ${RESTPORT}

Where RESTPORT is the PORT on which PostgREST is listening.

