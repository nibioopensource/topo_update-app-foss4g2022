#!/bin/sh

usage() {
  echo "Usage: $0 [--no-create-db] [--drop-app] <dbname>"
}

DO_CREATE_DB=yes
DO_DROP_APP=no

while [ $# -gt 0 ]
do
    case "$1" in
    (--no-create-db) DO_CREATE_DB=no;;
    (--drop-app) DO_DROP_APP=yes;;
    (--) shift; break;;
    (-*) echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
    (*)  break;;
    esac
    shift
done

test -n "$1" || {
  usage >&2
  exit 1
}

cd $(dirname $0)
BASEDIR=$(pwd -P)

# Ensure submodules are initialized
test -e pgtopo_update_sql/Makefile -a -e pgtopo_update_rest/Makefile || {
  {
    echo "Submodules are missing, please run:"
    echo "git -C $BASEDIR submodule update --init"
  } >&2
  exit 1
}

#test -f pgtopo_update_sql/topo_update.sql || {
  echo "Building pgtopo_update_sql/topo_update.sql"
  make -C pgtopo_update_sql || exit 1
#}

#test -f pgtopo_update_rest/topo_update_rest.sql || {
  echo "Building pgtopo_update_rest/topo_update_rest.sql"
  make -C pgtopo_update_rest || exit 1
#}

export PGDATABASE="$1"
PSQL="psql -XtAq --set ON_ERROR_STOP=1"
if [ "${DO_CREATE_DB}" = "yes" ]; then
  ${PSQL} -c "CREATE DATABASE \"${PGDATABASE}\"" template1 || exit 1
fi
${PSQL} -c "CREATE EXTENSION IF NOT EXISTS postgis_topology CASCADE" || exit 1
${PSQL} -f pgtopo_update_sql/topo_update.sql || exit 1
${PSQL} -f pgtopo_update_rest/topo_update_rest.sql || exit 1
${PSQL} -f dbsetup/create_roles.sql || exit 1
if [ "${DO_DROP_APP}" = "yes" ]; then
  {
    echo "Dropping a topo_update app is not currently supported"
    echo "See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/227"
  } >&1
  exit 1
fi
${PSQL} -f dbsetup/generate_app.sql || exit 1
${PSQL} -f dbsetup/grant_privs.sql || exit 1

cat <<EOF

Preparation of database ${PGDATABASE} is completed.
You can test it with:

    ./test_db.sh ${PGDATABASE}

Make sure to grant 'topology_update_app-foss4g-editor'
role to the users you want to be able to edit the demo data.

EOF
