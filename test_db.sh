#!/bin/sh

export PGDATABASE="$1"

echo "Testing preparation of ${PGDATABASE}"
pg_prove --dbname "${PGDATABASE}" || exit 1
echo "Database of ${PGDATABASE} is verified"


