#!/bin/sh

usage() {
  echo "Usage: $0 <dbname>"
}

test -n "$1" || {
  usage >&2
  exit 1
}
export PGDATABASE="$1"

BASEDIR=$(cd $(dirname $0) && pwd -P)
LOGFILE=/tmp/postgrest-$$.log

PORT=
if test -n "$PGRST_SERVER_PORT"; then
  PORT=$PGRST_SERVER_PORT
else
  export PGRST_SERVER_PORT=0
fi

#postgrest ${BASEDIR}/postgrest.conf > ${LOGFILE} 2>&1 &
export PGRST_DB_URI="postgres://@/"
export PGRST_DB_ANON_ROLE=topology_update_app-foss4g-editor
export PGRST_DB_SCHEMA=topo_update_rest
postgrest > ${LOGFILE} 2>&1 &
PID=$!

echo "PID=${PID}"
echo "LOGFILE=${LOGFILE}"

attempts=2
while :; do
  sleep 1
  grep -q 'Listening on port' ${LOGFILE} && break
  attempts=$((attempts-1))
  if test $attempts -eq 0; then
    echo "Server was unable to listen after 10 attempts." >&2
    echo "Last 10 lines of ${LOGFILE}:" >&2
    tail ${LOGFILE} >&2
    echo "Killing server process" >&2
    kill ${PID}
    exit 1
  fi
  echo "Waiting for process to start listening (${attempts})" >&2
done

if test -z "${PORT}"; then
  # PORT=$(
  #   netstat -tnpl 2> /dev/null |
  #     grep "LISTEN .* ${PID}/" |
  #     awk '{print $4}' |
  #     cut -d: -f2
  # )
  # # netstat is not available by default (net-tools)
  # PORT=$(
  #   ss -tnpl 2> /dev/null |
  #     grep "pid=${PID}," |
  #     awk '{print $4}' |
  #     cut -d: -f2
  # )
  # ss is not available on mac
  PORT=$(
    lsof -i 4 -n -T -c postgrest -p $PID -a |
      tail -1 | awk '{print $9}' | cut -d: -f2
  )
fi
echo "PORT=${PORT}"
