-- Grant application editor privileges to given role name
-- TODO: move this upstream, see
--       https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/239
--
CREATE OR REPLACE FUNCTION public._grant_topo_update_app_privs_editor(
  app_name TEXT,
  editor_role_name NAME
)
RETURNS VOID
AS $BODY$
BEGIN

  EXECUTE FORMAT(
    $$

GRANT SELECT, INSERT, UPDATE, DELETE
  ON TABLE %1$s.border
  TO %2$I;

GRANT SELECT, INSERT, UPDATE, DELETE
  ON TABLE %1$s.surface
  TO %2$I;

GRANT SELECT, INSERT, DELETE, UPDATE
  ON ALL TABLES IN SCHEMA %4$I
  TO %2$I;

GRANT USAGE
  ON SCHEMA %3$I
  TO %2$I;

GRANT SELECT
  ON TABLE %3$I.app_config
  TO %2$I;

-- These are required for topo_update_rest.get_features_topojson

GRANT USAGE
  ON SCHEMA topo_update
  TO %2$I;

GRANT USAGE
  ON SCHEMA %1$s
  TO %2$I;

GRANT USAGE
  ON SCHEMA %4$I
  TO %2$I;

-- These are required for topo_update_rest.add_borders_split_surfaces

GRANT USAGE
  ON ALL SEQUENCES IN SCHEMA %4$I
  TO %2$I;

GRANT USAGE
  ON ALL SEQUENCES IN SCHEMA %1$s
  TO %2$I;
    $$,
    app_name,
    editor_role_name,
    format('%s_sysdata_webclient_functions', app_name),
    format('%s_sysdata_webclient', app_name)
  );

END
$BODY$ LANGUAGE 'plpgsql' VOLATILE STRICT;

-- Grant application reader privileges to given role name
--
-- TODO: move this upstream, see
--       https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/239
--
CREATE OR REPLACE FUNCTION public._grant_topo_update_app_privs_reader(
  app_name TEXT,
  editor_role_name NAME
)
RETURNS VOID
AS $BODY$
BEGIN

  EXECUTE FORMAT(
    $_GRANTS_$

GRANT SELECT
  ON TABLE %1$s.border
  TO %2$I;

GRANT SELECT
  ON TABLE %1$s.surface
  TO %2$I;

GRANT SELECT
  ON ALL TABLES IN SCHEMA %4$I
  TO %2$I;

GRANT USAGE
  ON SCHEMA %3$I
  TO %2$I;

GRANT SELECT
  ON TABLE %3$I.app_config
  TO %2$I;

-- These are required for topo_update_rest.get_features_topojson

GRANT USAGE
  ON SCHEMA topo_update
  TO %2$I;

GRANT USAGE
  ON SCHEMA %1$s
  TO %2$I;

GRANT USAGE
  ON SCHEMA %4$I
  TO %2$I;

    $_GRANTS_$,
    app_name,
    editor_role_name,
    format('%s_sysdata_webclient_functions', app_name),
    format('%s_sysdata_webclient', app_name)
  );

END
$BODY$ LANGUAGE 'plpgsql' VOLATILE STRICT;

SELECT public._grant_topo_update_app_privs_editor('foss4g', 'topology_update_app-foss4g-editor');

SELECT public._grant_topo_update_app_privs_reader('foss4g', 'topology_update_app-foss4g-unauthenticated');

-- Grant topology permission ( would not be needed with PostGIS-3.2+ )
GRANT SELECT ON topology.topology TO PUBLIC;
GRANT SELECT ON topology.layer TO PUBLIC;
GRANT USAGE ON SCHEMA topology TO PUBLIC;
