-- Role of a foss4g unauthenticated user

DO $do$
DECLARE
  v_rolename TEXT := 'topology_update_app-foss4g-unauthenticated';
BEGIN
   IF EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE rolname = v_rolename
  )
  THEN
    RAISE NOTICE 'Role % already exists', v_rolename;
    RETURN;
  END IF;

  EXECUTE format($$
    CREATE ROLE %I
      NOSUPERUSER
      NOCREATEDB
      NOCREATEROLE
      NOLOGIN
      NOINHERIT
  $$, v_rolename)
  ;
  RAISE NOTICE 'Created role %', v_rolename;

END
$do$;

-- Role of a foss4g demo editor
DO $do$
DECLARE
  v_rolename TEXT := 'topology_update_app-foss4g-editor';
BEGIN
   IF EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE rolname = v_rolename
  )
  THEN
    RAISE NOTICE 'Role % already exists', v_rolename;
    RETURN;
  END IF;

  EXECUTE format($$
    CREATE ROLE %I
      NOSUPERUSER
      NOCREATEDB
      NOCREATEROLE
      NOLOGIN
      NOINHERIT
  $$, v_rolename)
  ;
  RAISE NOTICE 'Created role %', v_rolename;

END
$do$;

-- Role of the foss4g authenticator user

DO $do$
DECLARE
  v_rolename TEXT := 'topology_update_app-foss4g-authenticator';
BEGIN
   IF EXISTS (
      SELECT FROM pg_catalog.pg_roles
      WHERE rolname = v_rolename
  )
  THEN
    RAISE NOTICE 'Role % already exists', v_rolename;
    RETURN;
  END IF;

  EXECUTE format($$
    CREATE ROLE %I
      NOSUPERUSER
      NOCREATEDB
      NOCREATEROLE
      NOLOGIN
      NOINHERIT
  $$, v_rolename)
  ;
  RAISE NOTICE 'Created role %', v_rolename;

END
$do$;

GRANT "topology_update_app-foss4g-unauthenticated"
TO "topology_update_app-foss4g-authenticator";

GRANT "topology_update_app-foss4g-editor"
TO "topology_update_app-foss4g-authenticator";
