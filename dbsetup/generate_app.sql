SELECT * FROM topo_update.app_CreateSchema(
	'foss4g',
	regexp_replace(
	$$
		{
			"version": "1.0",
			"topology": {
				"srid": 3003,
				/* 1cm tolerance */
				"snap_tolerance": 1e-2
			},
			"tables": [
				{
					"name": "surface",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "serial"
						},
						{
							"name": "label",
							"type": "text"
						}
					],
					"topogeom_columns": [
						{
							"name": "tg",
							"type": "areal"
						}
					]
				},
				{
					"name": "border",
					"primary_key": "id",
					"attributes": [
						{
							"name": "id",
							"type": "serial"
						},
						{
							"name": "label",
							"type": "text"
						}
					],
					"topogeom_columns": [
						{
							"name": "tg",
							"type": "lineal"
						}
					]
				}
			],
			"surface_layer": {
				"table_name": "surface",
				"geo_column": "tg",
				"version_column": "id"
			},
			"border_layer": {
				"table_name": "border",
				"geo_column": "tg",
				"version_column": "id"
			},
			"operations": {
				"AddBordersSplitSurfaces": {
					"parameters": {
						"default": {
							"snapTolerance": {
								"units": 1e-2
							},
							"minAllowedNewSurfacesArea": {
								"units": 100 /* square meters */
							},
							"maxAllowedSplitSurfacesCount": 1,
							"maxAllowedNewSurfacesCount": 2
						},
						"forOpenLines": {
							"minAllowedNewSurfacesArea": {
								"units": 10
							},
							"maxAllowedNewSurfacesCount": 3
						}
					}
				},
				"SurfaceMerge": { },
				"UpdateAttributes": { },
				"GetFeaturesAsTopoJSON": { }
			}
		}
	$$,
	-- strip comments
	'\/\*.*?\*\/', '', 'g'
	)::jsonb
);

